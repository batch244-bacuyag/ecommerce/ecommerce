import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductView() {

	// The "useParams" hook allows us to retrieve the courseId passed via the URL
	const { productId } = useParams();

	//to be able to obtain the user ID so that we can enroll a user
	const {user} = useContext(UserContext)

	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ id, setId ] = useState('');
	const [ price, setPrice ] = useState(0);
	const [quantity, setQuantity] = useState(1);
	const [imageUrl, setImageUrl] = useState('');
	const formattedPrice = Number(price).toLocaleString('en-PH', { style: 'currency', currency: 'PHP' });
	
	useEffect(() => {
		console.log(productId);

		// Fetch request that will retrieve the details of the course from the database to be displayed in the "CourseView" page
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setId(data.id)
			setImageUrl(data.imageUrl)

		})
		.catch(error => console.log(error));
	}, [productId]);


	//Function to add to cart
	const addToCart = (productId, quantity) =>{
		console.log(productId)

		fetch(`${process.env.REACT_APP_API_URL}/users/cart`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity,
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data.message === "Product added to cart") {
				Swal.fire({
					title: "Successfully added",
					icon: "success",
					text: "You have successfully added the product to your cart."
				})
			} else if (data.message === "Product quantity updated") {
				Swal.fire({
					title: "Quantity updated",
					icon: "success",
					text: "You have successfully updated the quantity of the product in your cart."
				})
			} else if (data.message === "Product removed from cart") {
				Swal.fire({
					title: "Successfully removed",
					icon: "success",
					text: "You have successfully removed the product from your cart."
				})
			} else if (data.message === "Product has been removed and your cart is now empty.") {
				Swal.fire({
					title: "Successfully removed",
					icon: "success",
					text: "Product has been removed and your cart is now empty."
				})
			} else if (data.message === "Unable to remove product. Your cart is empty.") {
				Swal.fire({
					title: "No product on cart",
					icon: "error",
					text: "Unable to remove product. Your cart is empty."
				})
			} else if (data.message === "Item is not on cart") {
				Swal.fire({
					title: "Item is not on cart",
					icon: "error",
					text: "Unable to remove product. Item is not on cart."
				})
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}



	return (

		<Container className="mt-5">
		  <Row>
		    <Col lg={{ span: 6, offset: 3 }}>
		      <Card>
		        <div>
		          <div style={{ display: 'flex', alignItems: 'center', padding: '1rem 1rem 1rem' }}>
		          	<div style={{ flex: '0 0 auto', marginRight: '20px'}}>
		          		<Card.Img src={imageUrl} style={{ height: '250px', objectFit: 'contain' }} />
		          	</div>
		          	<div style={{ flex: '1 1 auto',  textAlign: 'center'  }}>
		          		<Card.Title>{name}</Card.Title>
		          		<Card.Subtitle>Price {formattedPrice}</Card.Subtitle>
		          		<br></br>
						<div style={{ display: 'flex', justifyContent: 'center' }}>
						    <Form className="d-flex align-items-center">
						        <Button variant="outline-secondary" onClick={() => setQuantity(quantity === 1 ? 0 : Math.max(quantity - 1, 1))}>-</Button>
						            <Form.Control type="number" min="0" className="mx-2" value={quantity} onChange={(e) => setQuantity(+e.target.value)} style={{ width: '55px' }}/>
						        <Button variant="outline-secondary" onClick={() => setQuantity(quantity + 1)}>+</Button>
						    </Form>
						</div>

		          		<br></br>
		          		{user.id !== null || user.isAdmin === false
		            		?
		            		<Button variant="primary" onClick={() => addToCart(productId, quantity)}>Add to cart</Button>
		            		:
		            		<Button as={Link} to="/login" variant="danger">Log in to add to cart</Button>
		          		}
		          	</div>
		          </div>
		          
		          <Card.Subtitle style={{ paddingLeft: '1rem'}}>Description</Card.Subtitle>
		          <Card.Text style={{ paddingLeft: '1rem', paddingRight: '1rem', paddingTop: '1rem', paddingBottom: '1rem' }} className="text-justify">{description}</Card.Text>

		        </div>
		      </Card>
		    </Col>
		  </Row>
		</Container>

	)
}