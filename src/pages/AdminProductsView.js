import { useEffect, useState } from 'react';
import AdminProductCard from '../components/AdminProductCard';
import Table from 'react-bootstrap/Table';


export default function AdminProductsView() {
  // State that will be used to store the products retrieved from the database
  const [products, setProducts] = useState([]);
  const [updateCounter, setUpdateCounter] = useState(0);

  // Retrieves the products from the database upon initial render of the component
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
      .then(res => res.json())
      .then(data => {
        console.log(data);

        // Sets the "products" state to map the data retrieved from the fetch request into several "AdminProductCard" components
        setProducts(data);
      })
  }, [updateCounter])

  const handleUpdateProduct = () => {
    // update product logic here
    setUpdateCounter(updateCounter + 1); // increment updateCounter to trigger a rerender
    console.log(updateCounter)
  }

  return (
    <>
      <h3>Products</h3>
      <Table striped bordered hover variant="light-blue">
        <thead>
          <tr>
            <th style={{ width: '25%' }}>Product Name</th>
            <th style={{ width: '25%' }}>Description</th>
            <th style={{ width: '10%' }}>Price</th>
            <th style={{ width: '20%' }}>Image URL</th>
            <th style={{ width: '7%' }}>Availablity</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody >
          {products.map(product => (
            <AdminProductCard key={product._id} product={product} onUpdate={handleUpdateProduct} />
          ))}
        </tbody>
      </Table>
    </>
  );
}

