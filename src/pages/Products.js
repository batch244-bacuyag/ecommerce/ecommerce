import { useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard';


export default function Products() {

	// State that will be used to store the products retrieved from the database
	const [ product, setProduct ] = useState([]);

	// Retrieves the products from the database upon intial render of the "Products" component

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			// Sets the "products" state to map the data retrieved from the fetch request into several "ProductCard" components
			setProduct(data.map(product => {
				return (
					<div key={product._id} className="col-lg-3 col-md-4 col-sm-12 mb-4 product-card animate__animated animate__fadeIn">
						<ProductCard productProp={product} />
					</div>
				)
			}))
		})
	}, [])

	return (
		<>
			<h1>Products</h1>
			<br></br>
			<div className="row">
				{product}
			</div>
		</>
	)
}
