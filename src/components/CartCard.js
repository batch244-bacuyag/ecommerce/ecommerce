import { useState, useEffect } from 'react';
import {Button, Form} from 'react-bootstrap';


export default function CartCard({ productProp, updateCart}) {
  const { productId, productName, quantity, pricePerItem, totalPrice, imageUrl } = productProp;
  const formattedPrice = Number(pricePerItem).toLocaleString('en-PH', { style: 'currency', currency: 'PHP' });
  const totalFormattedPrice = Number(totalPrice).toLocaleString('en-PH', { style: 'currency', currency: 'PHP' });
  const [quantityState, setQuantityState] = useState(quantity);
  const [count, setCount] = useState(0)

  const handleQuantityChange = (event) => {
    const newQuantity = parseInt(event.target.value);
    setQuantityState(newQuantity);
  }

  const handleClick = async() => {
    const reponse = await addToCart(productId, 0);
    updateCart()
  };

 const addToCart = async (productId, quantity) =>{
    console.log(productId)
    const reponse = await fetch(`${process.env.REACT_APP_API_URL}/users/cart`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        productId: productId,
        quantity: quantity
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);
    })
    updateCart()

  }

  return (
    <>
      <tr key={productName}>
        <td>
        <div style={{ textAlign: 'center' }}>
          <img src={imageUrl} alt={productName} style={{ height: '120px', objectFit: 'contain' }} />
          <h4>{productName}</h4>
          <p>{formattedPrice}</p>
        </div>
        </td>
        <td style={{ textAlign: 'center' }}>
          <Form style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <Form.Control type="number" min="0" value={quantityState} onChange={handleQuantityChange} className="text-center" inputMode="numeric" style={{ width: '100px', margin: '0 5px', fontSize: '16px' }} />
          </Form>
        </td>
        <td style={{ textAlign: 'center' }}>{totalFormattedPrice}</td>
        <td style={{ textAlign: 'center' }}>
        <Button variant="primary" className="mt-3 me-2" onClick={() => addToCart(productId, quantityState)}>
          Update
        </Button>
        <Button variant="danger" className="mt-3" onClick={handleClick}>
          Remove
        </Button>
        </td>
      </tr>
    </>
  );
}



