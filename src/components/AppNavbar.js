import { useContext } from 'react';
import { Navbar, Container, Nav } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import pic from '../techtonic.PNG';
import './AppNavbar.css';
import SearchIcon from '@mui/icons-material/Search';
import ShoppingCartRoundedIcon from '@mui/icons-material/ShoppingCartRounded';
import NavDropdown from 'react-bootstrap/NavDropdown';

export default function AppNavbar() {
  const { user } = useContext(UserContext);

  return (
    <Navbar bg="white" expand="lg">
      <Container>
        <Navbar.Brand as={Link} to="/" show>
          <div style={{ display: 'flex', alignItems: 'center', flexWrap: 'wrap' }}>
            <img className="navbar_logo" src={pic} />
          </div>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" className="ml-auto" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto flex-grow-1 justify-content-end">
            {!user.isAdmin && (
              <Nav.Link as={NavLink} to="/products" className="product-link">
                <span className="zoom">Product</span>
              </Nav.Link>
            )}

            <>

              <Nav>
                <NavDropdown
                  id="nav-dropdown-dark-example"
                  title={<span className="zoom">Access</span>}
                  menuVariant="light"
                >
                  {user.id !== null ? (
                    user.isAdmin === false ? (
                      <>
                        <NavDropdown.Item as={NavLink} to="/logout">
                          Logout
                        </NavDropdown.Item>
                        <NavDropdown.Item as={NavLink} to="/users/myAccount">
                          Account Information
                        </NavDropdown.Item>
                      </>
                    ) : (
                      <>
                        <NavDropdown.Item as={NavLink} to="/logout">
                          Logout
                        </NavDropdown.Item>
                        <NavDropdown.Item as={NavLink} to="/products/add">
                          Add Product
                        </NavDropdown.Item>
                        <NavDropdown.Item as={NavLink} to="/products/all">
                          All Products
                        </NavDropdown.Item>
                        <NavDropdown.Item as={NavLink} to="/users">
                          Users
                        </NavDropdown.Item>
                      </>
                    )
                  ) : (
                    <>
                      <NavDropdown.Item href="/login">Login</NavDropdown.Item>
                      <NavDropdown.Item href="/register">
                        Don't have an account? Register
                      </NavDropdown.Item>
                    </>
                  )}
                </NavDropdown>
              </Nav>
            </>

            {!user.isAdmin && (
              <Nav.Link as={NavLink} to="/cart">
                <ShoppingCartRoundedIcon className="navbar_shoppingCartIcon" />
              </Nav.Link>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
